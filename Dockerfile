# set base image (host OS)
FROM python:3.8

# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY Pipfile .
COPY Pipfile.lock .

# install dependencies
RUN pip install pipenv
RUN pipenv install

# copy the content of the local src directory to the working directory
COPY . .

EXPOSE 5000

# command to run on container start
CMD ["pipenv", "run", "flask", "run", "--host=0.0.0.0"]