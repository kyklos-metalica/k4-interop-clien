from tokenize import PlainToken
from flask import Flask, request
import json
import requests
#from models.scan_job import ScanJob
import os 
from  kyklos40_client import Kyklos40_client
 

class ScanJob:
    def __init__(self, Pilot: str, Subsystems: float, Data: list):
        self.Pilot = Pilot
        self.Subsystems = Subsystems
        self.Data = Data


app = Flask(__name__)
kyklos = Kyklos40_client() 

gvs1 = 0
gldd1 = 0

@app.route('/')
def hello():
    return "This is the Device Manager!"

def create_json(ts,vs1,ldd1):

    message = """{
      "Pilot": "METALICA",
      "Subsystems": "pipe1",
      "Data": [
        {
          "TS": "%s",
          "VS1": %s,
          "LDD1": %s
        }    
      ]
    }
    """%( ts,round(vs1,6),round(ldd1,1))
    return message


@app.route('/notification', methods=['POST'])
def notification():
    global gvs1, gldd1
    if request.data:    
        notification = request.get_json()

        #subscriptionId = notification['subscriptionId']
        #id = notification['data'][0]['id']
        #type= notification['data'][0]['type']
        #pilot= notification['data'][0]['Pilot']

        if 'data' not in notification:
          ValueError("No data for target")
          message = {"message" : "No data for target!"}
          return message

        data= notification['data'][0]['Data']

        ts= data[0]['TS']
        

        if 'VS1' not in data[0]:
          vs1=gvs1
        else:
          vs1=data[0]['VS1']
          gvs1=vs1

        if 'LDD1' not in data[0]:
          ldd1=gldd1
        else:
          ldd1=data[0]['LDD1']
          gldd1=ldd1
 
        msg = create_json(ts,vs1,ldd1)
        kyklos.publish(msg)
        return notification
         
    else:
        message = {"message" : "No notifications!"}
        message = json.dumps(message, indent=2)

        print(message, flush=True)
        return message

if __name__ == '__main__':
    app.run(host="127.0.0.1", port=5000)
