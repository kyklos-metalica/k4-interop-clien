import datetime
import random
import time
import os

from paho.mqtt import client as mqtt_client


class Kyklos40_client:
  """
    Kyklos 4.0 Interoperability client. It interacts with the Kyklos 4.0 API 
  """
  broker = 'kyklos-backend.kyklos40project.eu'
  port = 1883
  topic = "metalica/pipe1"
  client_id = 'sensor-001'
  username = os.getenv('KYKLOS_40_USERNAME') 
  password = os.getenv('KYKLOS_40_PASSWORD') 

  def __init__(self):    
    self.client = self.connect_mqtt()
    self.client.loop_start()

  def connect_mqtt(self):
    def on_connect(client, userdata, flags, rc):
        print("Trying to connect...")
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(self.client_id)
    client.username_pw_set(self.username, self.password)
    client.on_connect = on_connect
    client.connect(self.broker, self.port)
    return client

  def publish(self,msg):
    """
    Send json message to Kyklos 4.0 API via MQTT
    """
    result = self.client.publish(self.topic, msg)
    status = result[0]
    if status == 0:
      print(f"Send `{msg}` to topic `{self.topic}`", flush=True)
    else:
      print(f"Failed to send message to topic {self.topic}", flush=True)


def run():
    client = Kyklos40_client()
    msg = """{
      "Pilot": "METALICA",
      "Subsystems": "pipe1",
      "Data": [
        {
          "TS": "2022-03-14 16:43:40",
          "VS1": 2780.420308,
          "LDD1": 4.5
        }    
      ]
      }
    """
    client.publish(msg)


if __name__ == '__main__':
    run()
