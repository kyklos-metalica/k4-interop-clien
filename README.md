# KYKLOS 4.0 Interoperability client

## Purpose
KYKLOS 4.0 Interoperability clien is used in Metalica project to sent the signals from Metalica backend to KYKLOS 4.0 Interoperability Layer.
 

## API endpoints

| Endpoint | HTTP Request | Explanation |
|   ---    |     ---      |     ---     |
| `/notification` | `POST` | Receive notification sent by Orion Context Broker |

## Run
To deploy the registry, execute the following command:
```bash
kubectl apply -f config/k8s/combo.yml
```
