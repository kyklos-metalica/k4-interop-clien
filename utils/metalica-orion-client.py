from models.orion.device import Device as OrionDevice
from models.iot_agent.device import Device as IotAgentDevice
from models.iot_agent.service import Service
import helper.orion.utils as orion_utils
import helper.iot_agent.utils as iot_agent_utils
import os


def get_signal():
    # Metalica example
    service = Service("??????????", "Metalica-Data", "")

    #response = iot_agent_utils.create_service(service, "openiot", "/")
    #print(response.text)

    response = iot_agent_utils.get_services("openiot", "/*")
#    print(response.text)

    device = IotAgentDevice("1", "Metalica-Data", [{'name': 'Pilot', 'type': 'Text'},
                                            {'name': 'Subsystems', 'type': 'Text'}, 
                                            {'name': 'Data', 'type': 'StructuredValue'}])

    #response = iot_agent_utils.create_device(device, "openiot", "/")
    #print(response.text)
    
#    response = iot_agent_utils.get_devices("openiot", "/")
#    print(response.text)
#    print(response.json())


    response = iot_agent_utils.get_device_details("device1", "openiot", "/")
#    print(response.text)


    response = iot_agent_utils.get_subscriptions("openiot")
    print(response.text)





def main():    
    """
    Functions used to interact with IoT Agent
    """
    get_signal()

    
    
if __name__ == "__main__":
    main()
