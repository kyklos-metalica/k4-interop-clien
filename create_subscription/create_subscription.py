import os
from traceback import print_tb
import requests
import json

class Subscribe:
    def __init__(self, description: str, subject: dict, notification:dict, expires: str = "", throttling: int = 0):
        self.description = description
        self.subject = subject
        self.notification = notification
        self.expires = expires
        self.throttling = throttling
        

def subscribe():
    """
    The Device Manager subscribes to receive notifications for all the devices
    :param: None
    :return: the response of the POST request
    """
    manager_ip = os.getenv('DEVICE_MANAGER')
    
    entities = {"entities": [{"idPattern": ".*", "typePattern": ".*"}]} # receive notifications for all devices, regardless its id or type
    condition = {"condition": {"attrs": [ "Subsystems", "Data"]}} # triggers for notification
    not_url = {"http": {"url": manager_ip}} # where to send the notification
    not_attrs = {"attrs": ("Pilot","Subsystems", "Data")} # the contents of the notification
    attrs_format = {"attrsFormat" : "keyValues"} # the attributes will have key-value format

    des = "Subscription of Metalica device manager for sensors"
    subject = {**entities, **condition}
    notification = {**not_url, **not_attrs, **attrs_format}

    sub = Subscribe(des, subject, notification)
    subscribe = to_json(sub)

    headers = {
        "Content-Type": "application/json",
        "Fiware-Service": "openiot"
    }
    url = form_url()

    response = requests.post(url, data=subscribe, headers=headers)

    return response

def to_json(obj):
    """
    Turn body of request to JSON format
    :param obj: the body of the request
    :return: JSON formatted body
    """
    request = json.dumps(obj.__dict__, indent=3)
    return request


def form_url():
    """
    Form the URL for the requests
    :param: None
    :return: the appropriate URL
    """
    url = os.getenv('ORION_URL')
    url = f"{url}/v2/subscriptions"

    return url

def main():
    response = subscribe()
    print(response.text)

if __name__ == "__main__":
    main()